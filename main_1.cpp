int log = 0;
bool debug = 0;
// get the children database
Rcpp::List children_db = Rcpp::as<Rcpp::List>(r_children_db);
std::unordered_map<std::string, std::set<std::string>> d_children = hmm::list_to_map(children_db);
// get the genes database
Rcpp::List genes_db = Rcpp::as<Rcpp::List>(r_genes_db);
std::unordered_map<std::string, std::set<std::string>> d_genes = hmm::list_to_map(genes_db);
std::string root = Rcpp::as<std::string>(r_root);
hmm::Tree tree;
tree.build_graph(root, d_children, d_genes, log, debug);
tree.build_tree(log, debug);
return tree.r_tree();
