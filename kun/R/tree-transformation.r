#2nd step
#transform the GO DAG to a tree, eliminate the multi-parent situation
#top down approach, because prune process is an upward procedure and I can assume all the parent-up don't have multi-parents

# tree nodes are coded as components, for they are parts/subsets of original DAG node

#results:
#nTree: number of tree nodes
#treePaIndex: tree parent index
#treeChildrenIndexMat: matrix for children index, each column represents one node
#treeChildrenCount: count of children
#componMappingMat: mapping between GO.id (DAG) to components, matrix of nMerge * nTree. If an element is 1 at x row y column, it means tree node y is a component of DAG node x.
#treeProp: proportion of each node to its parent in term number of genes

#########################################
######## start
#########################################
#assume GO.id is calculated and structures(M, nParents, ChildrenIndexMat, nChild) exist
#continue running from 1.GO.structure.R
dag2tree = function(go.dag)
    #-------------------------------------------------------------------------
    XX = go.dag$XX
    #-------------------------------------------------------------------------
    logfile <- "single.parent.log.txt"
    #back up everything, since the structure will change
    XX.bak <- XX
    M.bak <- M
    pcount.bak <- pcount
    childrenIndexMat.bak <- childrenIndexMat
    childrenCount.bak <- childrenCount

    #add extra space to childrenIndexMat, some of the node may have more children at the end.
    childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=40, ncol=nMerge))

    #some functions

    #test if a is a subset of b
    is.subset <- function(a, b){
        if(sum(a %in% b) == length(a)) return(TRUE)
        return(FALSE)
    }

    getAncestorsIndex <- function(id){
        if(id==1) return(c())
        candidate <- M[1:pcount[id] , id]
        ancestors <- c()
        while(1){
            if(length(candidate)==0) break
            target <- candidate[1]
            ancestors <- union(ancestors, target)
            if(target != 1){
                candidate <- c(candidate, M[1:pcount[target] , target])
            }
            candidate <- setdiff(candidate, ancestors)
        }
        return(unique(sort(ancestors)))
    }

    #check pairwisely if one node is another's ancestor
    #could save some time if we can store the result from getAncestors
    rm.shortcut <- function(ids){
        retInd <- rep(TRUE, length(ids))
        for(ri in 1:length(ids)){
            origin <- ids[ri]
            for(rj in 1:length(ids)){
                if(rj!=ri){
                    peer <- ids[rj]
                    if(origin %in% getAncestorsIndex(peer)){
                        retInd[ri] <- FALSE
                        break
                    }
                }
            }
        }
        return(ids[retInd])
    }

    #getDifferential
    #return genes that are in pa(parent) but not only in ids (could be a vector)
    #i.e. parent-id, but still need to include all other children
    getDiff <- function(pa, ids, XX){
        result <- XX[[pa]]
        for(iid in 1:length(ids)){
            result <- setdiff(result, XX[[ids[iid]]])
        }
        for(gi in 1:childrenCount[pa]){
            childId <- childrenIndexMat[gi, pa]
            if(!(childId %in% ids)) result <- union(result, XX[[childId]])
        }
        return(result)
    }

    #checkDifferential
    #check if we can remove the children, redundant child can't remove any gene
    #return TRUE if pa and ids can be differentiated
    checkDiff <- function(pa, ids, XX){
        return(!setequal(getDiff(pa, ids, XX), XX[[pa]]))
    }

    # redundant child: how to choose which parent to keep
    # longest path to common ancestor so that the change is minimized
    # provided that we can find a sibling in each node that can break the redundancy
    # 

    #get path from pa until it hit kAncestor
    #assume only one parent upwards
    #getLongPath include the merging node
    getLongPath <- function(pa, kAncestor, M){
        path <- c(pa)
        while(!(pa %in% kAncestor)){
            pa <- M[1, pa]
            path <- c(path, pa)
        }
        return(path)
    }

    #getPath include only the nodes need to be trimmed
    #get path from pa unitil right before it hit kAncestor
    getPath <- function(pa, kAncestor, M){
        path <- c()
        while(!(pa %in% kAncestor)){
            path <- c(path, pa)
            pa <- M[1, pa]
        }
        return(path)
    }

    # need to write a function that check a list of parent, find their common ancestor and keep the longest path one
    #

    #find the single sibling that together they break the redundancy
    findSibling <- function(child, parent, XX, childrenCount, childrenIndexMat){
        listChildren <- setdiff(childrenIndexMat[1:childrenCount[parent] , parent], child)
        #order other children by size, from small to large
        sizeChildren <- sapply(listChildren, function(x) length(XX[[x]]))
        the.order <- sort.list(sizeChildren, decreasing = FALSE)
        listChildren <- listChildren[the.order]
        for(j in 1:length(listChildren)){
            #exit if the other child don't share any gene
        if(length(intersect(XX[[child]], XX[[listChildren[j]]])) ==0) next  
            otherChildren <- setdiff(listChildren, listChildren[j])
            unionOtherChildren <- c()
            for(k in 1:length(otherChildren)){
                unionOtherChildren <- union(unionOtherChildren, XX[[otherChildren[k]]])
            }
            if(is.subset(XX[[child]], unionOtherChildren)) next
            if(is.subset(XX[[listChildren[j]]], unionOtherChildren)) next
            return(listChildren[j])
        }
        return(-1)
    }

    # update unique gene (ug) from original child as we go along the path at node pa, 
    # i.e. the genes are in the original child but not in any 
    getUniqueGene <- function(ug, pa, oldpa, XX){
        result <- c()
        for(gi in 1:childrenCount[pa]){
            childId <- childrenIndexMat[gi, pa]
            if(childId != oldpa) result <- union(result, XX[[childId]])
        }
        return(setdiff(ug, result))
    }                                                               


    #choose keeper
    #2009.01.04 version
    #look through all the path to calculate a rough moving cost
    #the moving cost penalize heavily (*10) if the child can't be taken out alone (no unique gene left)
    chooseKeepers <- function(parents, child, XX, M){
        movingCost <- rep(0, length(parents))
        for(i in 1:length(parents)){
            pa <- parents[i]
            pAncestor <- getAncestorsIndex(pa)
            for(j in setdiff(parents, pa)){
                path <- getPath(j, pAncestor, M)
                if(!checkDiff(j, child, XX)){
                    movingCost[i] <- movingCost[i] + 10*length(path)
                }else{
                    if(length(path) == 1){
                        movingCost[i] <- movingCost[i] + 1
                    }else{
                        UniqueGene <- setdiff(XX[[j]], getDiff(j, child, XX))
                        for(k in 2:length(path)){
                            UniqueGene <- getUniqueGene(UniqueGene, path[k], path[k-1], XX)
                            if(length(UniqueGene) == 0){
                                movingCost[i] <- movingCost[i] + 10*(length(path)-k+1)
                                break
                            }else{
                                movingCost[i] <- movingCost[i] + 1
                            }
                        }
                    }
                }
            }
        }
        the.order <- order(movingCost, childrenCount[parents], nSize[parents], decreasing=FALSE)
        return(parents[the.order])
    }

    #check whether removeSet is a legit set to remove from parent
    checkRemoveSet <- function(pa, removeSet, XX){
        leftover <- getDiff(pa, removeSet, XX)
        #comment below out because sometimes removeSet are already good
        #if(setequal(leftover, XX[[pa]])) return(FALSE)
        for(i in removeSet){
            if(is.subset(XX[[i]], leftover)) return(FALSE)
        }
        return(TRUE)
    }

    #check the consistency of M and childrenIndexMat, good for diagnostics
    checkConsistency <- function(){
        result <- TRUE
        if(ncol(M)!=ncol(childrenIndexMat)){
            cat("!!!\n")
            return(FALSE)
        }
        
        #check from M to childrenIndexMat
        for(i in 2:ncol(M)){
            for(j in 1:pcount[i]){
                parent <- M[j, i]
                if(!(i %in% childrenIndexMat[1:childrenCount[parent], parent])){
                    cat(parent, " -> ", i, " link doesn't exist in childrenIndexMat\n")
                    result <- FALSE
                }
            }
        }
        
        #check from childrenIndexMat to M
        for(i in 1:ncol(M)){
            if(childrenCount[i]>=1){
                for(j in 1:childrenCount[i]){
                    child <- childrenIndexMat[j, i]
                    if(!(i %in% M[1:pcount[child], child])){
                        cat(i, " -> ", child, " link doesn't exist in M\n")
                        result <- FALSE
                    }
                }
            }
        }
        return(result)
    }

    #check if the componMappingMat can restore the original GO.id gene sets
    #GO.id gene sets are in XX.bak, components gene sets are in XX
    checkMappingMat <- function(componMappingMat){
        for(i in 1:nMerge){
            componList <- (1:nCompon)[componMappingMat[i,]==1]
            geneSet <- c()
            for(j in componList){
                geneSet <- union(geneSet, XX[[j]])
            }
            if(!setequal(XX.bak[[i]], geneSet)){
                cat(i,"th GO.id can't be recovered from components\n")
                return(FALSE)
            }
        }
        return(TRUE)
    }

    #calculate childrenIndexMat from M
    getChildrenIndexMat <- function(M, pcount, nNew){
        myChildrenIndexMat <- matrix(0, nrow=max(childrenCount)+10, ncol=nNew)
        myChildrenCount <- rep(0, nNew)
        for(i in 2:nNew){
            parents <- M[1:pcount[i], i]
            for(j in parents){
                myChildrenCount[j] <- myChildrenCount[j]+1
                myChildrenIndexMat[myChildrenCount[j], j] <- i
            }
        }
        for(i in 1:nNew){
            if(myChildrenCount[i]>0){
                myChildrenIndexMat[1:myChildrenCount[i], i] <- sort(myChildrenIndexMat[1:myChildrenCount[i], i])
            }
        }
        myChildrenIndexMat <- myChildrenIndexMat[1:max(myChildrenCount), ]
        return(list(childrenIndexMat=myChildrenIndexMat, childrenCount=myChildrenCount))
    }

    # actually go through the path, calculate measures/moving cost in term of component or total gene change.
    trimPath <- function(target, keeper, parents, joblist, i){
        totalCompon <- sum(componCount)
        totalGene <- sum(sapply(XX, length))
        
        kAncestor <- getAncestorsIndex(keeper)
        parents <- setdiff(parents, keeper)	

        operatedNode <- c()
        
        for(j in 1:length(parents)){
            pa <- parents[j]
            longPath <- getLongPath(pa, kAncestor, M)
            path <- longPath[1:(length(longPath)-1)]
            if(length(intersect(operatedNode, path))>0){
                stop("path overlap with operated nodes.")
            }else{
                operatedNode <- union(operatedNode, path)
            }
            endingNode <- longPath[length(longPath)]
            removeSet <- c(target)
            newParents <- setdiff(M[1:pcount[target], target], pa)
            pcount[target] <- length(newParents)
            M[, target] <- c(newParents, rep(0, (nrow(M)-length(newParents))))
            for(pa in path){
                if(!checkRemoveSet(pa, removeSet, XX)){
                    simpleSolution <- FALSE
                    if(length(removeSet) == 1){
                        sibling <- findSibling(target, pa, XX, childrenCount, childrenIndexMat)
                        if(sibling!=-1){   #found it
                            removeSet <- c(removeSet, sibling)
                            #replace sibling's current parent with endingNode
                            M[1:pcount[sibling], sibling][match(pa, M[1:pcount[sibling], sibling])] <- endingNode
                            simpleSolution <- TRUE
                        }
                    }
                    #otherwise, we search for other child to bring with us
                    if(!simpleSolution){
                        listChildren <- setdiff(childrenIndexMat[1:childrenCount[pa] , pa], target)
                        sizeChildren <- sapply(listChildren, function(x) length(XX[[x]]))
                        sizeOverlapChildren <- sapply(listChildren, function(x) length(intersect(XX[[x]], XX[[target]])))
                        #order the children by the number of overlaping genes with target, break tie by total number of genes
                        the.order <- order(sizeOverlapChildren, -sizeChildren, decreasing = TRUE)
                        listChildren <- listChildren[the.order]
                        # the worst case is remove all the child
                        for(k in listChildren){
                            removeSet <- union(removeSet, k)
                            M[1:pcount[k], k][match(pa, M[1:pcount[k], k])] <- endingNode
                            if(checkRemoveSet(pa, removeSet, XX)) break
                        }
                    }
                }#end if(!checkRemoveSet(pa, removeSet, XX))
                temp <- getDiff(pa, removeSet, XX)

                XX[[pa]] <- temp
                # detach from current parent
                newChildList <- sort(setdiff(childrenIndexMat[1:childrenCount[pa] , pa], removeSet))
                childrenCount[pa] <- length(newChildList)
                if(childrenCount[pa] > nrow(childrenIndexMat)) childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=(childrenCount[pa] - nrow(childrenIndexMat) + 10), ncol=nCompon))
                childrenIndexMat[, pa] <- c(newChildList, rep(0, (nrow(childrenIndexMat)-length(newChildList))))
                #add components to pa
                componCount[pa] <- componCount[pa]+length(removeSet)
                #if(componCount[pa] > nrow(compon)) compon <- rbind(compon, matrix(0, nrow=(componCount[pa] - nrow(compon) + 10), ncol=nCompon))
                #compon[(componCount[pa]-length(removeSet)+1):componCount[pa], pa] <- removeSet
            }#end for(pa in path){
            #get the merging node
            pa <- M[1, pa]
            newChildList <- sort(union(childrenIndexMat[1:childrenCount[pa] , pa], setdiff(removeSet, target)))
            childrenCount[pa] <- length(newChildList)
            if(childrenCount[pa] > nrow(childrenIndexMat)) childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=(childrenCount[pa] - nrow(childrenIndexMat) + 10), ncol=nCompon))
            childrenIndexMat[, pa] <- c(newChildList, rep(0, (nrow(childrenIndexMat)-length(newChildList))))
            
            #need to rm shortcut again if removeSet has joblist member in it
            shortcutList <- intersect(removeSet, joblist[(i+1):length(joblist)])
            if(length(shortcutList)>0){
                for(l in 1:length(shortcutList)){
                    scTarget <- shortcutList[l]
                    scParents <- M[1:pcount[scTarget], scTarget]
                    temp <-	rm.shortcut(scParents)
                    if(length(temp) < length(scParents)){ #exist shortcut in current parents
                        cutoff <- setdiff(scParents, temp)
                        #cat("inline shortcut. target: ", scTarget, "; shortcut: ", cutoff, "\n", sep=" ", file=logfile, append=TRUE)
                        #remove shortcut parents
                        pcount[scTarget] <- length(temp)
                        M[,scTarget] <- c(temp, rep(0, (nrow(M)-length(temp))))
                        for(j in 1:length(cutoff)){
                            cutParent <- cutoff[j]
                            #remove self as child
                            childrenIndexMat[, cutParent] <- c(setdiff(childrenIndexMat[1:childrenCount[cutParent] , cutParent], scTarget), rep(0, (nrow(childrenIndexMat)-childrenCount[cutParent]+1)))
                            childrenCount[cutParent] <- childrenCount[cutParent]-1
                        }
                    }
                }
            }
            #if(!checkConsistency()) stop("consistency")
        }#end for(j in 1:length(parents))
        componScore <- sum(componCount) - totalCompon
        geneScore <- totalGene - sum(sapply(XX, length))
        return(list(geneScore=geneScore, componScore=componScore, XX=XX, pcount=pcount, M=M, childrenCount=childrenCount, childrenIndexMat=childrenIndexMat, componCount=componCount))
    }

    #save.image("temp.Rdata")



    ######################################################################################################################

    # debug mode, if true, will log actions
    debug <- FALSE

    #big loop: remove shortcut, remove multiple parents, delete null nodes, merge clones, complete the graph, reorder the nodes
    #number of components
    nCompon <- nMerge

    cat("Start at ", date(), "\n", file=logfile)

    countBetterKeeper <- 0

    while(1){

    #backup??
    #XX.old <- XX

    #####*****#####
    # step 1
    # remove existing shortcuts
    # shortcut example: A is parent for both B and C, but B is also parent of C. Thus the A->C relationship is redundant.
    cat("Before rm.shortcut, total links: ", sum(pcount), "\n", sep="")
    # remove shortcuts
    joblist <- (1:nCompon)[pcount>1]
    if(length(joblist) == 0) break
    if(length(joblist) > 0){
        # first round: eliminate existing shortcut
        for(i in 1:length(joblist)){
            target <- joblist[i]
            parents <- M[1:pcount[target], target]
            temp <-	rm.shortcut(parents)
            if(length(temp) < length(parents)){ #exist shortcut in current parents
                cutoff <- setdiff(parents, temp)
                #cat("target: ", target, "; shortcut: ", cutoff, "\n", sep=" ", file=logfile, append=TRUE)
                #remove shortcut parents
                pcount[target] <- length(temp)
                M[,target] <- c(temp, rep(0, (nrow(M)-length(temp))))
                for(j in 1:length(cutoff)){
                    cutParent <- cutoff[j]
                    #remove self as child
                    childrenIndexMat[, cutParent] <- c(setdiff(childrenIndexMat[1:childrenCount[cutParent] , cutParent], target), rep(0, (nrow(childrenIndexMat)-childrenCount[cutParent]+1)))
                    childrenCount[cutParent] <- childrenCount[cutParent]-1
                }
            }
        }
    }#end if
    cat("After rm.shortcut, total links: ", sum(pcount), "\n", sep="")

    #save.image("temp1.Rdata")

    #####*****#####
    # step 2
    # remove multiple parents
    #components
    cat("Step 2 begin, totalGene: ", sum(sapply(XX, length)), "\n", sep="")
    #compon <- matrix(0, nrow=200, ncol=nCompon)
    #compon[1,] <- 1:nCompon
    componCount <- rep(1, nCompon)

    cat("\n", rep("*", 20), "\n", file=logfile, append=TRUE, sep="")
    cat("Start removing multiple parents at ", date(), "\n", file=logfile, append=TRUE)
    joblist <- (1:nCompon)[pcount>1]
    #breaking point
    if(length(joblist) == 0) break
    nullNodeList <- c()
    for(i in 1:length(joblist)){
    step <- 1
        target <- joblist[i]
        if(pcount[target] == 1) next
        #cat("###node:", target, "\n", file=logfile, append=TRUE)
        parents <- M[1:pcount[target], target]
        
        componScore <- Inf
        geneScore <- Inf
        
        keepers <- chooseKeepers(parents, target, XX, M)
        
        for(keeper in keepers){
            result <- trimPath(target=target, keeper=keeper, parents=parents, joblist=joblist, i=i)
            if(result$componScore < componScore){
                if(componScore < Inf) countBetterKeeper <- countBetterKeeper + 1 #beat my keeper
                #if(result$componScore > componScore) cat("Inconsistency: target=", target, "; keeper=", keeper, 
                #"; Gene.old=", geneScore, "; Gene=", result$geneScore, "; Compon.old=", componScore, "; Compon=", result$componScore, 
                #"\n", file="measure.txt", sep="", append=TRUE)
                
                geneScore <- result$geneScore
                componScore <- result$componScore
                XX.best <- result$XX
                pcount.best <- result$pcount
                M.best <- result$M
                childrenCount.best <- result$childrenCount
                childrenIndexMat.best <- result$childrenIndexMat
                componCount.best <- result$componCount
                #compon.best <- result$compon
                #if(!checkConsistency()) stop("consistency")
            }
        }#end for(keeper in keepers)
        XX <- XX.best
        pcount <- pcount.best
        M <- M.best
        childrenCount <- childrenCount.best
        childrenIndexMat <- childrenIndexMat.best
        componCount <- componCount.best
        #compon <- compon.best
    }#end for(i in 1:length(joblist))

    if(!checkConsistency()) stop("consistency")
    #if(!checkMappingMat()) stop("MappingMat")

    nSize <- sapply(XX, length)
    nullNodeList <- (1:nCompon)[nSize == 0]

    cat("Step 2 end, totalGene: ", sum(sapply(XX, length)), "\n", sep="")
    #save.image("temp2.Rdata")

    #####*****#####
    # step 3
    # delete nullNodeList
    # oldIndex is the mapping from new index to old index
    if(length(nullNodeList)>0){
        cat("Step 3: ", length(nullNodeList), " nullNodeList.\n", sep="")
        oldIndex <- setdiff((1:nCompon), nullNodeList)
        nNew <- length(oldIndex)
        # newIndex is the mapping from old index to new index
        newIndex <- rep(0, nCompon)
        for(i in 1:nNew){
            newIndex[oldIndex[i]] <- i
        }
        
        findNewIndex <- function(old){
            if(old == 0) return(0)
            if(old>nCompon) stop("old index should be between 1 and nCompon\n")
            return(newIndex[old])
        }
        
        XX <- XX[oldIndex]
        
        #we can use findNewIndex here since non-empty node's parant is not empty
        M.new <- matrix(0, nrow=nrow(M), ncol=nNew)
        for(i in 1:nNew){
            M.new[, i] <- sapply(M[, oldIndex[i]], findNewIndex)
        }
        M.old <- M
        M <- M.new
        pcount.old <- pcount
        pcount <- pcount[oldIndex]
        
        childrenIndexMat.new <- matrix(0, nrow=nrow(childrenIndexMat), ncol=nNew)
        childrenCount.new <- rep(0, nNew)
        
        for(i in 1:nNew){
            old <- oldIndex[i]
            newChildList <- sapply(sort(intersect(childrenIndexMat[1:childrenCount[old], old], oldIndex)), findNewIndex)
            len <- length(newChildList)
            childrenCount.new[i] <- len
            if(len>0){
                childrenIndexMat.new[1:len, i] <- newChildList
            }
        }
        childrenIndexMat.old <- childrenIndexMat
        childrenIndexMat <- childrenIndexMat.new
        childrenCount.old <- childrenCount
        childrenCount <- childrenCount.new
        
        nCompon.old <- nCompon
        nCompon <- nNew
        
        if(!checkConsistency()) stop("consistency")
        #if(!checkMappingMat()) stop("MappingMat")
    }#end if(length(nullNodeList)>0)

    cat("Step 3 end, total components: ", nCompon, "\n", sep="")
    #save.image("temp3.Rdata")

    #####*****#####
    # step 4
    # merge clones
    nSize <- sapply(XX, length)
    temp <- table(nSize)
    temp <- as.data.frame(temp)
    targetSize <- sort(round(as.numeric(as.character(temp[temp$Freq>1,1]))), decreasing = TRUE)

    nClusterSize <- rep(0, 500)
    cloneCluster <- matrix(rep(0, (500*20)), nrow=500, ncol=20, byrow=F)
    clusterIndex <- 0
    clone.all <- c()

    for(size in targetSize){
        sizeIndex <- (1:nCompon)[nSize==size]
        while(1){
            if(length(sizeIndex) <= 1) break
            head <- sizeIndex[1]
            rest <- sizeIndex[2:length(sizeIndex)]
            result <- sapply(rest, function(x) setequal(XX[[head]], XX[[x]]))
            if(sum(result) == 0){
                #no clone for first element
                sizeIndex <- rest
            }else{
                clusterIndex <- clusterIndex+1
                if(clusterIndex > length(nClusterSize)){
                    nClusterSize <- c(nClusterSize, rep(0, 10))
                    cloneCluster <- rbind(cloneCluster, matrix("", nrow=(nrow(cloneCluster)+10), ncol=ncol(cloneCluster)))
                }
                len <- sum(result)+1
                nClusterSize[clusterIndex] <- len
                if(len > ncol(cloneCluster)) cloneCluster <- cbind(cloneCluster, matrix("", nrow=nrow(cloneCluster), ncol=(len - ncol(cloneCluster) + 10)))
                cloneCluster[clusterIndex, 1:len] <- c(head, rest[result])
                clone.all <- union(clone.all, c(head, rest[result]))
                sizeIndex <- rest[!result]
            }
        }
    }

    is.clone <- function(cid){
        cid %in% clone.all
    }

    noClone <- TRUE
    if(clusterIndex >0){
        cat("Step 4: ", clusterIndex, " clones.\n", sep="")
        noClone <- FALSE
        nClusterSize <- nClusterSize[1:clusterIndex]
        cloneCluster <- cloneCluster[1:clusterIndex, 1:max(nClusterSize)]
        
        #cloneHeadIndex map the clones to its representative, the head
        cloneHeadIndex <- rep(0, nCompon)
        if(clusterIndex>1){
            clone.head <- as.vector(cloneCluster[,1])
            for(i in 1:clusterIndex){
                for(j in 1:nClusterSize[i]){
                    cloneHeadIndex[cloneCluster[i,j]] <- cloneCluster[i,1]
                }
            }
        }else{ #clusterIndex==1
            clone.head <- cloneCluster[1]
            for(j in 1:nClusterSize[1]){
                cloneHeadIndex[cloneCluster[j]] <- cloneCluster[1]
            }
        }
        
        # oldIndex is the mapping from new index to old index
        oldIndex <- setdiff((1:nCompon), setdiff(clone.all, clone.head))
        nNew <- length(oldIndex)
        # newIndex is the mapping from old index to new index
        # the clones are mapped to the new index of their clone head
        newIndex <- rep(0, nCompon)
        for(i in 1:nCompon){
            if(is.clone(i)){
                newIndex[i] <- (1:nNew)[oldIndex == cloneHeadIndex[i]]
            }else{
                newIndex[i] <- (1:nNew)[oldIndex == i]
            }
        }
        
        XX.old <- XX
        XX <- XX[oldIndex]
        
        #rebuild M and childrenIndexMat
        M.new <- matrix(0, nrow=nrow(M)+10, ncol=nNew)
        pcount.new <- rep(0, nNew)
        checked <- rep(FALSE, nCompon)
        for(i in 2:nCompon){
            if(checked[i]) next
            newID <- newIndex[i]
            if(!is.clone(i)){
                #not a clone
                newParents <- sort(unique(newIndex[M[1:pcount[i], i]]))
                checked[i] <- TRUE
            }else{
                #a clone, need to merge all the parents of the clone member
                cloneList <- (1:nCompon)[cloneHeadIndex == cloneHeadIndex[i]]
                newParents <- c()
                for(j in cloneList){
                    newParents <- union(newParents, M[1:pcount[j], j])
                }
                newParents <- sort(unique(setdiff(newIndex[newParents], newID)))
                checked[cloneList] <- TRUE
            }
            pcount.new[newID] <- length(newParents)
            M.new[1:pcount.new[newID], newID] <- newParents
        }
        M.new <- M.new[1:max(max(pcount.new), 2),]
        M.old <- M
        M <- M.new
        pcount.old <- pcount
        pcount <- pcount.new
        
        childrenIndexMat.old <- childrenIndexMat
        childrenCount.old <- childrenCount
        ret <- getChildrenIndexMat(M, pcount, nNew)
        childrenIndexMat <- ret$childrenIndexMat
        childrenCount <- ret$childrenCount
        
        nCompon.old <- nCompon
        nCompon <- nNew
        
        if(!checkConsistency()) stop("consistency")
        #if(!checkMappingMat()) stop("MappingMat")
    }#end if(clusterIndex >0)

    cat("Step 4 end, total components: ", nCompon, "\n", sep="")
    #save.image("temp4.Rdata")


    #####*****#####
    # step 5
    # complete the graph for missing parental relationships

    nSize <- sapply(XX, length)

    M <- rbind(M, matrix(0, nrow=30, ncol=nCompon))
    childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=30, ncol=nCompon))

    counter <- 0
    for(i in 2:nCompon){
        #we only look at those whose size is bigger and it is not in ancestors and descendants
        ancestors <- getAncestorsIndex(i)
        candidate <- setdiff((1:nCompon)[nSize>nSize[i]], ancestors)
        if(length(candidate) == 0) next
        #i is a subset of candidate
        candidate <- candidate[sapply(candidate, function(x) is.subset(XX[[i]], XX[[x]]))]
        if(length(candidate) == 0) next
        #candidate should not have descendants that is also candidate
        candidate <- candidate[sapply(candidate, function(x) length(intersect(candidate, getDescendantsIndex(x))) == 0)]
        if(length(candidate) == 0) next
        
        counter <- counter + length(candidate)
        for(j in candidate){
            #link i to the new parents
            pcount[i] <- pcount[i] + 1
            if(pcount[i] > nrow(M)) M <- rbind(M, matrix(0, nrow=10, ncol=nCompon))
            M[1:pcount[i], i] <- sort(c(M[1:(pcount[i]-1), i], j))
            childrenCount[j] <- childrenCount[j] + 1
            if(childrenCount[j] > 1){
                if(childrenCount[j] > nrow(childrenIndexMat)) childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=10, ncol=nCompon))
                childrenIndexMat[1:childrenCount[j], j] <- sort(c(childrenIndexMat[1:(childrenCount[j]-1), j], i))
            }else{#children count ==1
                childrenIndexMat[1, j] <- i
            }
            cat(i, " is linked to ", j, "\n", sep="", file=logfile, append=TRUE)	
        }
    }

    M <- M[1:max(max(pcount),2),]
    childrenIndexMat <- childrenIndexMat[1:max(childrenCount),]

    cat("Step 5 end. Total ", counter, " links been added.\n", sep="")
    noMissingParent <- counter==0

    if(!checkConsistency()) stop("consistency")

    #save.image("temp5.Rdata")

    #####*****#####
    # step 6
    # reorder the nodes, since new links added

    if(noClone && noMissingParent) break

    headIndex <- 1
    tailIndex <- 1
    index <- 2
    parentCounter <- rep(1, nCompon)

    # oldIndex is the mapping from new index to old index, a permutation of 1:nCompon
    oldIndex <- rep(-1, nCompon)
    oldIndex[1] <- 1
    while(1){
        nChildren <- childrenCount[oldIndex[headIndex]]
        if(nChildren>0){
            for(j in 1:nChildren){
                childIndex <- childrenIndexMat[j, oldIndex[headIndex]]
                if(parentCounter[childIndex] < pcount[childIndex]){
                    parentCounter[childIndex] <- parentCounter[childIndex]+1
                }else{
                    if(parentCounter[childIndex] > pcount[childIndex]) cat("Things went wrong for node: ", oldIndex[headIndex], " children: ", j, "\n")
                    oldIndex[index] <- childIndex
                    index <- index+1
                }
            }
        }
        headIndex <- headIndex+1
        if(headIndex>tailIndex) tailIndex <- index-1    #next tier
        if(headIndex==index) break        #the end
        if(index > nCompon) break
    }

    # need to rebuild the M matrix
    # one way is to tranform the current M matrix
    # oldIndex is a permutation of 1:nCompon
    newIndex <- order(oldIndex)

    findNewIndex <- function(old){
        if(old == 0) return(0)
        if(old>nCompon) stop("old index should be between 1 and nCompon\n")
        return(newIndex[old])
    }

    M.new <- matrix(0, nrow=nrow(M), ncol=nCompon)
    for(i in 1:nNew){
        M.new[, i] <- sapply(M[, oldIndex[i]], findNewIndex)
    }
    M.old <- M
    M <- M.new
    pcount.old <- pcount
    pcount <- pcount[oldIndex]

    childrenIndexMat.old <- childrenIndexMat
    childrenCount.old <- childrenCount
    ret <- getChildrenIndexMat(M, pcount, nCompon)
    childrenIndexMat <- ret$childrenIndexMat
    childrenCount <- ret$childrenCount

    XX <- XX[oldIndex]

    if(!checkConsistency()) stop("consistency")

    #save.image("temp6.Rdata")

    }#end while(1)
    cat("End at ", date(), "\n", file=logfile, append=TRUE)

    ####################################

    nTree <- nCompon
    treePaIndex <- M[1,]
    treeChildrenIndexMat <- childrenIndexMat
    treeChildrenCount <- childrenCount

    treeProp <- rep(-1, nTree)
    geneCount <- sapply(1:nTree, function(x) length(XX[[x]]))
    treeProp[2:nTree] <- sapply(2:nTree, function(x) geneCount[x]/geneCount[treePaIndex[x]])

    DAG.size <- sapply(XX.bak, length)
    tree.size <- sapply(XX, length)

    #check proper subset
    for(i in 2:nTree){
        if(is.subset(XX[[i]], XX[[treePaIndex[i]]]) && length(XX[[i]]) < length(XX[[treePaIndex[i]]])) next
        cat(i, "\n")
    }

    #check for clone
    for(i in 1:nTree){
        candidate <- (1:nTree)[-i][tree.size[-i]==tree.size[i]]
        if(length(candidate)==0) next
        if(sum(sapply(candidate, function(x) setequal(XX[[i]], XX[[x]])))>0) cat(i, "\n")
    }


    # compute a order that could be used in upward recursion
    leaf <- c(1:nTree)[treeChildrenCount==0]
    nonleaf <- c()
    candidate <- c(1:nTree)[treeChildrenCount==0]

    #bottom-up search
    childCounter <- rep(1, nTree)
    while(1){
        if(length(candidate)==0) break
        target <- candidate[1]
        if(target != 1){
            parentIndex <- treePaIndex[target]
            if(childCounter[parentIndex] < treeChildrenCount[parentIndex]){
                childCounter[parentIndex] <- childCounter[parentIndex]+1
            }else{
                #update current parent
                #add current parent into candidate
                candidate <- c(candidate, parentIndex)
                nonleaf <- c(nonleaf, parentIndex)
            }
        }
        candidate <- setdiff(candidate, target)
    }

    depth <- rep(0, nTree)
    for(i in 2:nTree){
      #if(treePaIndex[i]!=1 && depth[treePaIndex[i]]==0) cat("!!!\n")
      depth[i] <- depth[treePaIndex[i]]+1
    }

    #################################
    # after transform the DAG to a tree, direct search for component in a top-down fashion

    #get path from pa to root, 1
    getPath2Top <- function(id){
        pa <- treePaIndex[id]
      path <- c(pa)
        while(pa != 1){
            pa <- treePaIndex[pa]
            path <- c(path, pa)
        }
        return(path)
    }

    getTreeChildren <- function(id){
        if(treeChildrenCount[id]==0) return(c())
        return(treeChildrenIndexMat[1:treeChildrenCount[id], id])
    }

    #mapping between GO.id to components
    componMappingMat <- matrix(0, nrow=nMerge, ncol=nTree)
    componMappingMat[1,1] <- 1
    date()
    candidate.init <- treeChildrenIndexMat[1:treeChildrenCount[1], 1]
    DAG.size.order <- sort.list(DAG.size, decreasing=TRUE)
    for(i in DAG.size.order[-1]){
        while(1){
            if(sum(tree.size[candidate.init] > DAG.size[i])==0) break
            rmInd <- (1:length(candidate.init))[tree.size[candidate.init] > DAG.size[i]]
            for(j in rmInd){
                candidate.init <- c(candidate.init, getTreeChildren(candidate.init[j]))
            }
            candidate.init <- candidate.init[-rmInd]
        }
        candidate <- candidate.init
        componList <- c()
        while(length(candidate)>0){
            target <- candidate[1]
            if(is.subset(XX[[target]], XX.bak[[i]])){
                componList <- c(componList, target)
            }else{
                if(length(intersect(XX[[target]], XX.bak[[i]]))>0){
                    candidate <- c(candidate, getTreeChildren(target))
                }
            }
            candidate <- candidate[-1]
        }
        componMappingMat[i, componList] <- 1
    }
    date()

    #check if the components has ancestral relationship among themselves, they should not.
    for(i in 2:nMerge){
        componIndex <- (1:nTree)[componMappingMat[i,]==1]
        for(j in componIndex){
            if(length(intersect(getPath2Top(j), componIndex)) > 0) stop(paste("Ancestral relation btw ", j, " and other components.", sep=""))
        }
    }
    #nothing

    checkMappingMat(componMappingMat)


    #summary statistics
    cat("Number of components:", nCompon, "\n")
    cat("Total number of composing components:", sum(componMappingMat), "\n")
    cat("Total number of genes in components:", sum(sapply(XX, length)), "\n")

    list(nMerge=nMerge, nTree=nTree, treePaIndex=treePaIndex, treeChildrenIndexMat=treeChildrenIndexMat, 
         treeChildrenCount=treeChildrenCount, componMappingMat=componMappingMat, treeProp=treeProp)
}
