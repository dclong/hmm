# 1st step: extract the GO graph information from Bioconductor.
# The structual information in GO can be obtained through elements like GOBPCHILDREN and GOBPPARENTS. 
# But these information are not easy to manipulate (for tree transformation), 
# so I set out to extract them into matrices.

#Final results:
#GO.id: id for GO terms
#nMerge: number of GO terms, i.e., nMerge=length(GO.id)
#pcount: the number of parent nodes for each node
#M: parent matrix, ncol(M)=nMerge, each column contain the id(s) of parent node(s)
#childrenCount: the number of children nodes for each node
#childrenIndexMat: children matrix, ncol(childrenIndexMat)=nMerge, each column contain the id(s) of children node(s)

read.go = function(){

    #the minimum of genes a GO term need to have to qualify as a gene set
    sizelimit <- 1

    #load ALL data, won't be used until step 7
    data(ALL)

    # another way to build the list, go is a list of list
    go <- as.list(hgu95av2GO2ALLPROBES)

    #get rid of IEA
    go <- lapply(go, function(x) x[!is.na(names(x)) & (names(x) != "IEA")])

    # keep only unique features
    go <- lapply(go, function(x) unique(x))

    #####*****#####
    # step 1
    # build the GO nodes that is nonempty, roughly breadth-first
    # GOBPCHILDREN function, need GO
    maxlength <- 5000
    headIndex <- 1
    tailIndex <- 1
    index <- 2

    GOid.v1 <- rep("", maxlength)
    GOid.v1[1] <- "GO:0008150"        #biological process, root node of bp
    while(1){
        children <- GOBPCHILDREN[[GOid.v1[headIndex]]]
        if(!is.null(children)){
            for(j in 1:length(children)){
                #need to check if GO node has gene in it, and not in the list already
                if(length(go[[children[[j]]]]) >= sizelimit && sum(children[[j]]==GOid.v1[1:(index-1)]) == 0 ){
                    GOid.v1[index] <- children[[j]]
                    index <- index+1
                }
            }
        }
        headIndex <- headIndex+1
        if(headIndex>tailIndex) tailIndex <- index-1    #next tier
        if(headIndex==index) break        #the end
        if(index > maxlength) break
    }

    nNodes <- index-1
    GOid.v1 <- GOid.v1[1:nNodes]
    sorted.GOid <- sort(GOid.v1)
    nameIndex.v1 <- 1:nNodes
    names(nameIndex.v1) <- GOid.v1

    #shrink go further more
    go <- go[names(go) %in% GOid.v1]

    #####*****#####
    # step 2
    # get the count of legit parents
    # output: nParents
    nParents <- rep(1, nNodes)
    nParents[1] <- 0
    for(i in 2:nNodes){
        parents <- GOBPPARENTS[[GOid.v1[i]]]
        lenParents <- length(parents)
        if(lenParents > 1){
            pcount <- 0
            for(j in 1:lenParents){
                if(parents[[j]] %in% sorted.GOid){
                    pcount <- pcount+1
                }
            }
            nParents[i] <- pcount
        }
    }

    #####*****#####
    # step 3
    # reorder the GO term so that every term is after all his parents
    # GOBPCHILDREN, need GO
    headIndex <- 1
    tailIndex <- 1
    index <- 2
    parentCounter <- rep(1, nNodes)

    GOid <- rep("", nNodes)
    GOid[1] <- "GO:0008150"        #biological process, root node of bp
    while(1){
        children <- GOBPCHILDREN[[GOid[headIndex]]]
        if(!is.null(children)){
            for(j in 1:length(children)){
                if(children[[j]] %in% sorted.GOid){
                    oldIndex <- nameIndex.v1[[children[[j]]]]		#oldIndex is the index in GOid.v1
                    if(parentCounter[oldIndex] < nParents[oldIndex]){
                        parentCounter[oldIndex] <- parentCounter[oldIndex]+1
                    }else{
                        if(parentCounter[oldIndex] > nParents[oldIndex]) cat("Things went wrong for v1Index: ", v1Index, " children: ", children[[j]], "\n")
                        GOid[index] <- children[[j]]
                        index <- index+1
                    }
                }
            }
        }
        headIndex <- headIndex+1
        if(headIndex>tailIndex) tailIndex <- index-1    #next tier
        if(headIndex==index) break        #the end
        if(index > nNodes) break
    }

    nameIndex <- 1:nNodes
    names(nameIndex) <- GOid

    #####*****#####
    # step 4
    # find clones
    # clones are GO terms that has same set of probes/genes, possibly due to the difference genes are not on the current microarray chip

    #nSize: size of each GO term/gene set
    nSize <- sapply(GOid, function(x) length(go[[x]]))
    temp <- table(nSize)
    temp <- as.data.frame(temp)
    targetSize <- sort(round(as.numeric(as.character(temp[temp$Freq>1,1]))), decreasing = TRUE)

    nClusterSize <- rep(0, 1000)
    cloneCluster <- matrix(rep("", (1000*20)), nrow=1000, ncol=20, byrow=F)
    clusterIndex <- 0
    clone.all <- c()

    for(size in targetSize){
        sizeGOid <- GOid[nSize==size]
        while(1){
            if(length(sizeGOid) <= 1) break
            head <- sizeGOid[1]
            rest <- sizeGOid[2:length(sizeGOid)]
            result <- sapply(rest, function(x) setequal(go[[head]], go[[x]]))
            if(sum(result) == 0){
                #no clone for first element
                sizeGOid <- rest
            }else{
                clusterIndex <- clusterIndex+1
                if(clusterIndex > length(nClusterSize)){
                    nClusterSize <- c(nClusterSize, rep(0, 10))
                    cloneCluster <- rbind(cloneCluster, matrix("", nrow=(nrow(cloneCluster)+10), ncol=ncol(cloneCluster)))
                }
                len <- sum(result)+1
                nClusterSize[clusterIndex] <- len
                if(len > ncol(cloneCluster)) cloneCluster <- cbind(cloneCluster, matrix("", nrow=nrow(cloneCluster), ncol=(len - ncol(cloneCluster) + 10)))
                cloneCluster[clusterIndex, 1:len] <- c(head, rest[result])
                clone.all <- union(clone.all, c(head, rest[result]))
                sizeGOid <- rest[!result]
            }
        }
    }

    nClusterSize <- nClusterSize[1:clusterIndex]
    cloneCluster <- cloneCluster[1:clusterIndex, 1:max(nClusterSize)]

    #clone.head will be kept as the representative of all the clones
    clone.head <- as.vector(cloneCluster[,1])

    is.clone <- function(cid){
        cid %in% clone.all
    }

    index <- 1
    cloneNodeName <- rep("", length(clone.all))
    nameClusterIndex <- rep(-1, length(clone.all))
    for(i in 1:clusterIndex){
        for(j in 1:nClusterSize[i]){
            cloneNodeName[index] <- cloneCluster[i,j]
            nameClusterIndex[index] <- i
            index <- index+1
        }
    }
    names(nameClusterIndex) <- cloneNodeName

    #####*****#####
    # step 5
    # The end result will be a new GO.id, a new nParents(pcount), a parentMatrix M

    # We use the first GO term in each cluster as its representative
    GO.id <- setdiff(GOid, setdiff(clone.all, clone.head))
    nMerge <- length(GO.id)
    nameIndex.merge <- 1:nMerge
    names(nameIndex.merge) <- GO.id

    nRow <- 10*max(nParents)
    # M is sort of transposed so that dput(M) will look good in WinBUGS
    M <- matrix(rep(0, (nRow*nMerge)), nrow=nRow, ncol=nMerge, byrow=F)
    pcount <- rep(0, nMerge)
    # go through the original nodes list, since merged node's parents is the union of everybody's parents
    for(i in 2:nNodes){
        parents <- GOBPPARENTS[[GOid[i]]]
        lenParents <- length(parents)
        if(GOid[i] %in% GO.id){	#has a valid index in GO.id
            target.index <- nameIndex.merge[[GOid[i]]]
        }else{
            # origin/children is a clone shadow
            #clone shadow should point to the clone head
            target.index <- nameIndex.merge[[clone.head[nameClusterIndex[[GOid[i]]]]]]
        }
        for(j in 1:lenParents){
            if(parents[[j]] %in% sorted.GOid){
                if(parents[[j]] %in% GO.id){
                    parentIndex <- nameIndex.merge[[parents[[j]]]]
                }else{
                    #parent is a clone shadow
                    #clone shadow should point to the clone head
                    parentIndex <- nameIndex.merge[[clone.head[nameClusterIndex[[parents[[j]]]]]]]
                }
                if(!(parentIndex %in% M[1:pcount[target.index], target.index]) && (parentIndex!=target.index)){ #no duplicate parent
                    pcount[target.index] <- pcount[target.index]+1
                    M[pcount[target.index], target.index] <- parentIndex
                }
            }
        }
    }

    M <- M[1:max(pcount),]

    # Let's build the child index matrix since after removing the clone we can't rely on GOBPCHILDREN
    nRow <- 70
    childrenIndexMat <- matrix(rep(0, (nRow*nMerge)), nrow=nRow, ncol=nMerge, byrow=F)
    childrenCount <- rep(0, nMerge)
    # go through the original nodes list, since merged node's children is the union of everybody's children
    for(i in 1:nNodes){
        children <- GOBPCHILDREN[[GOid[i]]]
        lenChildren <- length(children)
        if(GOid[i] %in% GO.id){	#has a valid index in GO.id
            target.index <- nameIndex.merge[[GOid[i]]]
        }else{
            # origin/children is a clone shadow
            #clone shadow should point to the clone head
            target.index <- nameIndex.merge[[clone.head[nameClusterIndex[[GOid[i]]]]]]
        }
        for(j in 1:lenChildren){
            if(children[[j]] %in% sorted.GOid){
                if(children[[j]] %in% GO.id){
                    childrenIndex <- nameIndex.merge[[children[[j]]]]
                }else{
                    #child is a clone shadow
                    #clone shadow should point to the clone head
                    childrenIndex <- nameIndex.merge[[clone.head[nameClusterIndex[[children[[j]]]]]]]
                }
                if(!(childrenIndex %in% childrenIndexMat[1:childrenCount[target.index], target.index]) && (childrenIndex != target.index)){ #no duplicate child
                    childrenCount[target.index] <- childrenCount[target.index]+1
                    childrenIndexMat[childrenCount[target.index], target.index] <- childrenIndex
                }
            }
        }
    }

    #save some space
    childrenIndexMat <- childrenIndexMat[1:max(childrenCount),]


    #####*****#####
    # step 6.0
    # GO graph utility function

    #getAncestorsIndex, return all the ancestor for the node
    getAncestorsIndex <- function(id){
        if(id==1) return(c())
        candidate <- M[1:pcount[id] , id]
        ancestors <- c()
        while(1){
            if(length(candidate)==0) break
            target <- candidate[1]
            ancestors <- union(ancestors, target)
            if(target != 1){
                candidate <- c(candidate, M[1:pcount[target] , target])
            }
            candidate <- setdiff(candidate, ancestors)
        }
        return(unique(sort(ancestors)))
    }

    # return all ancestor for a list of nodes, including themselves
    getAllAncestorsIndex <- function(idv){
        if(length(idv)==0){
            return(c())
        }else{
            result <- c()
            for(i in 1:length(idv)){
                result <- union(result, getAncestorsIndex(idv[i]))
            }
        }
        return(sort(unique(union(result, idv))))
    }

    getDescendantsIndex <- function(id){
        if(childrenCount[id]==0) return(c())
        candidate <- childrenIndexMat[1:childrenCount[id] , id]
        descendants <- c()
        while(1){
            if(length(candidate)==0) break
            target <- candidate[1]
            descendants <- c(descendants, target)
            if(childrenCount[target] > 0){
                candidate <- c(candidate, childrenIndexMat[1:childrenCount[target] , target])
            }
            candidate <- setdiff(candidate, descendants)
        }
        return(unique(sort(descendants)))
    }

    getAllDescendantsIndex <- function(idv){
        if(length(idv)==0){
            return(c())
        }else{
            result <- c()
            for(i in 1:length(idv)){
                result <- union(result, getDescendantsIndex(idv[i]))
            }
        }
        return(sort(unique(union(result, idv))))
    }

    is.subset <- function(a, b){
        if(sum(a %in% b) == length(a)) return(TRUE)
        return(FALSE)
    }

    id.is.subset <- function(origin, target){
        return(is.subset(go[[GO.id[origin]]], go[[GO.id[target]]]))
    }

    #####*****#####
    # step 6.1
    # Reorganize the GO graph
    # Motivation: Since not all genes are on certain gene chip, there are subset relationships that are not in the GO graph structure.
    # Notably one node could become children of its original siblings. Or there could be missing relationships.
    # Plan: top-down update
    # Computation demanding!

    logFile <- "graph.complete.log.txt"

    nSize <- sapply(1:nMerge, function(x) length(go[[GO.id[x]]]))

    M <- rbind(M, matrix(0, nrow=80, ncol=nMerge))
    childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=80, ncol=nMerge))

    cat(date(), "\n", file=logFile)

    for(i in 2:nMerge){
        #we only look at those whose size is bigger and it is not in ancestors and descendants
        #cat(i, "\n", file=logFile, append=TRUE)
        ancestors <- getAncestorsIndex(i)
        descendants <- getDescendantsIndex(i)
        candidate <- setdiff((1:nMerge)[nSize>nSize[i]], ancestors)
        if(length(candidate) == 0) next
        #i is a subset of candidate
        candidate <- candidate[sapply(candidate, function(x) id.is.subset(i, x))]
        if(length(candidate) == 0) next
        #candidate should not have descendants that is also candidate
        candidate <- candidate[sapply(candidate, function(x) length(intersect(candidate, getDescendantsIndex(x))) == 0)]
        if(length(candidate) == 0) next
        
        for(j in candidate){
            #link i to the new parents
            pcount[i] <- pcount[i] + 1
            if(pcount[i] > nrow(M)) M <- rbind(M, matrix(0, nrow=10, ncol=nMerge))
            M[1:pcount[i], i] <- sort(c(M[1:(pcount[i]-1), i], j))
            childrenCount[j] <- childrenCount[j] + 1
            if(childrenCount[j] > 1){
                if(childrenCount[j] > nrow(childrenIndexMat)) childrenIndexMat <- rbind(childrenIndexMat, matrix(0, nrow=10, ncol=nMerge))
                childrenIndexMat[1:childrenCount[j], j] <- sort(c(childrenIndexMat[1:(childrenCount[j]-1), j], i))
            }else{#children count ==1
                childrenIndexMat[1, j] <- i
            }
            cat(i, " is linked to ", j, "\n", sep="", file=logFile, append=TRUE)	
        }
    }

    M <- M[1:max(pcount),]
    childrenIndexMat <- childrenIndexMat[1:max(childrenCount),]


    # need to reorder the node so that parents have smaller number than children

    #####*****#####
    # step 6.5
    # Reordering the nodes after removing clones
    headIndex <- 1
    tailIndex <- 1
    index <- 2
    parentCounter <- rep(1, nMerge)

    # oldIndex is the mapping from new index to old index, a permutation of 1:nMerge
    oldIndex <- rep(-1, nMerge)
    oldIndex[1] <- 1
    while(1){
        nChildren <- childrenCount[oldIndex[headIndex]]
        if(nChildren>0){
            for(j in 1:nChildren){
                childIndex <- childrenIndexMat[j, oldIndex[headIndex]]
                if(parentCounter[childIndex] < pcount[childIndex]){
                    parentCounter[childIndex] <- parentCounter[childIndex]+1
                }else{
                    if(parentCounter[childIndex] > pcount[childIndex]) cat("Things went wrong for node: ", oldIndex[headIndex], " children: ", j, "\n")
                    oldIndex[index] <- childIndex
                    index <- index+1
                }
            }
        }
        headIndex <- headIndex+1
        if(headIndex>tailIndex) tailIndex <- index-1    #next tier
        if(headIndex==index) break        #the end
        if(index > nMerge) break
    }

    pcount.v2 <- pcount
    pcount <- pcount[oldIndex]

    # need to rebuild the M matrix
    # by tranforming the current M matrix
    # oldIndex is a permutation of 1:nMerge
    ordering <- order(oldIndex)
    # so ordering is a map from old to new
    findIndex <- function(old){
        if(old==0){
            return(0)
        }else{
            return(ordering[old])
        }
    }

    M.v2 <- M

    M <- matrix(sapply(M[,oldIndex], findIndex), nrow=max(pcount), ncol=nMerge, byrow=F)

    GO.id.v2 <- GO.id
    GO.id <- GO.id[oldIndex]

    nameIndex.merge <- 1:nMerge
    names(nameIndex.merge) <- GO.id

    #mapping between GOid and GO.id
    mapping.GOid.2.GO.id <- rep(-1, length(GOid))
    mapping.GOid.2.GO.id[1] <- 1

    for(i in 2:nNodes){
        if(GOid[i] %in% GO.id){	#has a valid index in GO.id
            target.index <- nameIndex.merge[[GOid[i]]]
        }else{
            target.index <- nameIndex.merge[[clone.head[nameClusterIndex[[GOid[i]]]]]]
        }
        mapping.GOid.2.GO.id[i] <- target.index
    }

    # build gene set (XX) for GO.id
    ALLindex <- 1:length(go)
    names(ALLindex) <- names(go)
    XXindex <- rep(-1, length(GO.id))
    for(i in 1:length(GO.id)){
        XXindex[i] <- ALLindex[[GO.id[i]]]
    }

    XX <- go[XXindex]

    # build the child matrix for use in later step to control FDR
    childrenIndexMat.v2 <- childrenIndexMat
    childrenIndexMat <- matrix(sapply(childrenIndexMat[,oldIndex], findIndex), nrow=max(childrenCount), ncol=nMerge, byrow=F)

    childrenCount.v2 <- childrenCount
    childrenCount <- childrenCount[oldIndex]


    #####*****#####
    # step 6.6
    # Should we worry about the new ordering of id(parent's index should be smaller than children's)
    # First check
    for(i in 2:nMerge){
        for(j in 1:pcount[i])
            if(M[j,i]>i) cat("Node ", i, "'s ", j, "th parent has index bigger than him\n")
    }
    # nothing is wrong

    # return result in list
    list(go=go, XX=XX, GOid=GOid, GO.id=GO.id, nMerge=nMerge, nSize=nSize, pcount=pcount, 
         M=M, childrenCount=childrenCount, childrenIndexMat=childrenIndexMat, 
         cloneCluster=cloneCluster, nClusterSize=nClusterSize, clone.all=clone.all, clone.head=clone.head, 
         is.clone=is.clone, nameClusterIndex=nameClusterIndex, nameIndex.merge=nameIndex.merge, mapping.GOid.2.GO.id=mapping.GOid.2.GO.id)
}
