
###################################
#####*****#####
# compute p-val
# There are many possible ways to compute p-val, and the following uses globaltest. Permutation test was used in our paper

y=exprs(ALL)

simData <- function(n=9) 
{
  B=sample(1:95,2*n)
  T=sample(96:128,n)
  d=y[,B]
  d[genesInDEA,(n+1):(2*n)]=y[genesInDEA,T]
  return(d)
}

d <- simData()

library(globaltest)
pval <- p.value(globaltest(X=d, Y=as.factor(c(rep(1,9),rep(0,9))), genesets=XX))

