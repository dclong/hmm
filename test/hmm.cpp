#define CATCH_CONFIG_MAIN  
#include <iostream>
#include "catch.hpp"
#include "../algo.h"
#include "../graph/graph.h"

SCENARIO("Read and write unordered_map", "[unordered_map, read, write, IO]"){
    GIVEN("an unordered_map with 3 pairs of key-value"){
        std::unordered_map< std::string, std::set<std::string> > m1;
        m1["John"] = std::set<std::string>{"San Francisco", "Male", "58"};
        m1["Ben"] = std::set<std::string>{"Ames", "Male", "27"};
        m1["Lisa"] = std::set<std::string>{"Xiangfan", "Female", "28"};
        WHEN("the unordered_map is written into a binary file"){
            hmm::write_binary_map("map.bin", m1);
        }
        WHEN("the binary file is read into a new unordered_map"){
            auto m2 = hmm::read_binary_map("map.bin");
            THEN("the 2 unordered_map have the same content"){
                REQUIRE(m2.size() == m1.size());
                REQUIRE(m2["John"] == m1["John"]);
                REQUIRE(m2["Ben"] == m1["Ben"]);
                REQUIRE(m2["Lisa"] == m1["Lisa"]);
            }
        }
        /*
         best to also test the children and genes database here
        WHEN("the children database is read in to an unordered_map"){
            auto m3 = hmm::read_binary_map("../../data/children_3.1.2.bin");
            THEN("the children of node ... is ..."){
                REQUIRE(conditions);
            }
        }
        WHEN("the genes database is read in to an unordered_map"){
            auto m4 = hmm::read_binary_map("../../data/genes_3.1.2.bin");
            THEN("the genes of node ... is ..."){
                REQUIRE(conditions);
            }
        }
        */
    }
}
/*
 * use a subset of the original data to test build_graph ...
 */
/*
SCENARIO("Build graph", "[graph, build]"){
    GIVEN("a graph built on testing data"){
        // code to build the scenario
        REQUIRE(conditions);
    }
}
*/
/*
SCENARIO("Build graph", "[graph, build]"){
    GIVEN("an empty graph"){
        REQUIRE(conditions);
        WHEN("the graph is built using full data"){
            auto d_children = hmm::read_binary_map("../../data/children_3.1.2.bin");
            auto d_probes = hmm::read_binary_map("../../data/genes_3.1.2.bin");
            hmm::Graph graph;
            graph.build_graph("GO:0008150", d_children, d_probes);
            THEN("..."){
                REQUIRE(conditions);
            }
        }
    }
}
*/
