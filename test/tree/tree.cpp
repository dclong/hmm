#include <iostream>
#include "../../algo.h"
#include "../../graph/graph.h"
#include "../../tree.h"

int main(int argc, char **argv) {
    if(argc != 3){
        std::cout << "Usage: ./tree.out log_level debug\n\n"
            << "log_level 0: no logging information\n"
            << "log_level 1: minor logging information\n"
            << "log_level 2: more logging information\n"
            << "log_level 3: extensive logging information\n"
            << "log_level 4: huge amount of logging information\n\n"
            << "debug 0: turn off debug\n"
            << "debug 1: turn on debug\n\n";
        return 1;
    }
    int log = std::stoi(argv[1]);
    bool debug = std::stoi(argv[2]);
    // read the d_children database
    std::unordered_map< std::string, std::set<std::string> > d_children = hmm::read_binary_map("../../data/children_3.1.2.bin");
    // read the d_children database
    std::unordered_map< std::string, std::set<std::string> > d_probes = hmm::read_binary_map("../../data/genes_3.1.2.bin");
    hmm::Tree tree;
    tree.build_graph("GO:0008150", d_children, d_probes, log, debug);
    tree.build_tree(log, debug);
    // test offspring
    // int size = tree.root_unidentified().size();
    // std::cout << "Number of unidentified nodes for root: " << size << std::endl;
    return 0;
}
