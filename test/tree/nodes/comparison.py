import pandas

def read_clones(file_path):
    '''Read clones from a file.
    '''
    with open(file_path, "r") as f:
        clones = [sorted(line.split()) for line in f]
    #end with
    clones.sort()
    return clones
#end def

def shorten_nodes(nodes):
    for i,node in enumerate(nodes):
        if node.find("<") >= 0:
            nodes[i] = node[:10]
        #end if
    #end for
#end def

def read_nodes(file_path, short):
    '''Read nodes from a file.
    '''
    with open(file_path, "r") as f:
        nodes = [line.strip() for line in f.readlines()]
    #end with
    if short:
        shorten_nodes(nodes)
    #end if
    nodes.sort()
    return nodes
#end def

def find_clones(node, clones_list):
    node_short = node[:10]
    node_info = node[10:]
    for clones in clones_list:
        if node_short in clones:
            return [clone + node_info for clone in clones]
        #end if
    #end for
    return [node]
#end def

def read_all(nodes_file, clones_file, short):
    '''Read nodes and clones from files and combine them tegother.
    '''
    nodes = read_nodes(nodes_file, short)
    clones_list = read_clones(clones_file)
    for i,node in enumerate(nodes):
        nodes[i] = find_clones(node, clones_list)
    #end for
    nodes.sort()
    return nodes
#end def

def break_long_node(long_node):
    long_node = long_node.replace("<P:", ",")
    long_node = long_node.replace(">", "")
    long_node = long_node.replace("C: ", "")
    long_node = long_node.replace("S: ", "")
    info = long_node.split(", ")
    info[1] = int(info[1]) 
    info[2] = int(info[2]) 
    info[3] = int(info[3]) 
    return info
#end def

def data_frame(long_nodes):
    df = pandas.DataFrame([break_long_node(node) for node in long_nodes], columns=["name", "np", "nc", "size"])
    df.index = df.name
    df = df.drop("name", 1)
    return df
#end def
