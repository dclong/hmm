
# -*- coding: utf-8 -*-


import os
os.chdir("/home/dclong/archives/in_action/hmm/test/tree/nodes/4")

import sys
sys.path.append("..")
import comparison


kun_short = comparison.read_all("nodes_kun.txt", "clones_kun.txt", True)
ben_short = comparison.read_all("nodes_ben.txt", "clones_ben.txt", True)
print(kun_short == ben_short)


kun_long = comparison.read_all("nodes_kun.txt", "clones_kun.txt", False)
ben_long = comparison.read_all("nodes_ben.txt", "clones_ben.txt", False)
print(kun_long == ben_long)

flag = [kun_long[i]==ben_long[i] for i in range(len(kun_long))]
print(flag)
kun_long = [kun[0] for kun in kun_long]
ben_long = [ben[0] for ben in ben_long]
print([k[:10] for k in kun_long] == [b[:10] for b in ben_long])

df_kun = comparison.data_frame(kun_long)
df_ben = comparison.data_frame(ben_long)
df_diff = df_kun - df_ben

df_diff.describe()
Out[113]: 
                np           nc  size
count  7696.000000  7696.000000  7696
mean      1.121362     1.121362     0
std       3.263093     4.618980     0
min       0.000000     0.000000     0
25%       0.000000     0.000000     0
50%       0.000000     0.000000     0
75%       1.000000     1.000000     0
max      73.000000   199.000000     0