int log = 0;
bool debug = 0;
// read the d_children database
std::unordered_map<std::string, std::set<std::string>> d_children = hmm::read_binary_map("data/children_3.1.2.bin");
// read the d_children database
std::unordered_map<std::string, std::set<std::string>> d_probes = hmm::read_binary_map("data/genes_3.1.2.bin");
hmm::Tree tree;
tree.build_graph("GO:0008150", d_children, d_probes, log, debug);
tree.build_tree(log, debug);
return tree.r_tree();
// test offspring
// int size = tree.root_unidentified().size();
// std::cout << "Number of unidentified nodes for root: " << size << std::endl;
