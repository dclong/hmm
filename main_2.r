library(inline)
library(Rcpp)

src = paste(readLines('main_2.cpp'), collapse="\n")
includes = '#include "tree.h"'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x -I /home/dclong/archives/hmm/', settings$env$PKG_CXXFLAGS)
fx = cxxfunction(body=src, includes=includes, settings=settings)
tree = fx()
