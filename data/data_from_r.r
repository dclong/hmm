#' This script write the 2 data objects generated by 
#' get_clean_probes (corresponding to d_genes below)
#' and get_clean_children (corresponding to d_children below)
#' to binary files
#' so that the development of C++ code is eaiser.
#' In the final code, 
#' it is not necessary to write these 2 data objects into files.
#' You just need to pass the corresponding map objects to the Rcpp code.
#'
library(hgu95av2.db)
library(GO.db)
library(Rcpp)
library(inline)

source("../r/get_clean_data.r")
get_clean_genes() -> d_genes
get_clean_children() -> d_children
get_clean_offspring() -> d_offspring

src = '
// convert R arguments input to C++ objects
Rcpp::List data = Rcpp::as<Rcpp::List>(r_data);
std::string file = Rcpp::as<std::string>(r_file);
// write data to file
write_binary_map(file.c_str(), list2map(data));
'
includes = paste(readLines('../rcpp_helper.h'), collapse="\n")
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x ', settings$env$PKG_CXXFLAGS)
write_data = cxxfunction(signature(r_data="list", r_file="character"), 
                         body=src, includes=includes, settings=settings)
write_data(d_children, "children_3.1.2.bin")
write_data(d_genes, "genes_3.1.2.bin")
write_data(d_offspring, "offspring_3.1.2.bin")

