## About

Gene category testing problems involve testing hundreds of null hypotheses 
that correspond to nodes in a directed acyclic graph. 
	The logical relationships among the nodes in the graph imply 
that only some configurations of true and false null hypotheses are possible 
and that a test for a given node should depend on data from neighboring nodes. 
Liang, Nettleton (2010) developed a method based on a hidden Markov model 
that takes the whole graph into account 
and provides coherent decisions in this structured multiple hypothesis testing problem. 
Here is a C++ implementation of the algorithm and an R package will be developed based on the C++ code.