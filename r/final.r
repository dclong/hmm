library(Rcpp)
library(inline)
src = '
const char * pfile = "/home/dclong/archives/code/cpp/hmm2/data/probes.bin";
const char * cfile = "/home/dclong/archives/code/cpp/hmm2/data/children.bin";
dclong::go_mcmc b;
auto probes = dclong::read_binary_map(pfile);
auto children = dclong::read_binary_map(cfile);
const char * root = "GO:0023052";
b.build_graph<true, false, true>(root, children, probes);
b.build_tree<true, true>();
std::string f_omega = "mcmc_omega.bin";
std::string f_alpha = "mcmc_alpha.bin";
std::string f_beta = "mcmc_beta.bin";
std::string f_state = "mcmc_state.bin";
b.mcmc(100, 10, f_omega, f_alpha, f_beta, f_state);
'
includes = '
#include "mcmc.hpp"
'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++11 -I /home/dclong/archives/code/cpp/hmm2/',settings$env$PKG_CXXFLAGS)
shuffle = cxxfunction(signature(), 
                      body=src, includes=includes, settings=settings)


