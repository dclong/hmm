
union_genes = function(nodes, tree){
    genes = NULL
    for(node in nodes){
        index = tree$genes_index[, node]
        index = index[index != 0]
        genes = union(genes, tree$all_genes[index])
    }
    genes
}

compare_genes = function(index, tree){
    nodes = tree$mapping[[index]]
    gs1 = union_genes(nodes, tree)
    gs2 = raw_genes[[tree$clones[[index]][1]]]
    setequal(gs1, gs2)
}

lapply(1:7696, function(x){compare_genes(x, tree)}) -> bs
all(unlist(bs))
