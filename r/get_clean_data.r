#' Convert hgu95av2.db::hgu95av2GO2ALLPROBES to a list and 
#' get rid of NAs and NULLs.
#'
#' @param availabe.probes an optional vector parameter containing 
#' all genes/probes used in a kind of microarray chip. 
#'
#' @param min.probes the minimum number of genes/probes in a node. 
#' The default value is 1.
#' 
#' @TODO: Kun asked me to remove "IEA", but do I also need to remove "all" flags?
#'
get_clean_genes = function(available.probes, min.probes=1){
    d_genes = as.list(hgu95av2.db::hgu95av2GO2ALLPROBES)
    for(i in length(d_genes):1){
        gi = d_genes[[i]]
        # drop "IEA"
        gi = gi[names(gi)!="IEA"]
        if(length(gi)==0 || all(is.na(gi))){
            d_genes[[i]] = NULL
            next
        }
        if(!missing(available.probes)){
            gi = base::intersect(gi, available.probes)
        }
        if(length(gi) < min.probes){
            d_genes[[i]] = NULL
            next
        }  
        d_genes[[i]] = gi
    }
    return(d_genes)
}

get_clean_children = function(){
    d_children = as.list(GO.db::GOBPCHILDREN)
    for(i in length(d_children):1){
        ci = d_children[[i]]
        ci = ci[names(ci)=="part_of" | names(ci)=="is_a"]
        if(length(ci)==0 || all(is.na(ci))){
            d_children[[i]] = NULL
            next
        }
        d_children[[i]] = ci
    }
    return(d_children)
}


get_clean_offspring = function(){
    d_offspring = as.list(GO.db::GOBPOFFSPRING)
    for(i in length(d_offspring):1){
        oi = d_offspring[[i]]
        if(length(oi)==0 || all(is.na(oi))){
            d_offspring[[i]] = NULL
            next
        }
        d_offspring[[i]] = oi
    }
    return(d_offspring)
}
