library(inline)
library(Rcpp)

src = paste(readLines('main_1.cpp'), collapse="\n")
includes = '#include "tree.h"'
settings=getPlugin("Rcpp")
settings$env$PKG_CXXFLAGS=paste('-std=c++0x -I /home/dclong/archives/hmm/', settings$env$PKG_CXXFLAGS)
fx = cxxfunction(signature(r_children_db="list", r_genes_db="list", r_root="character"), body=src, includes=includes, settings=settings)
tree = fx(get_clean_children(), get_clean_genes(), "GO:0008150")
